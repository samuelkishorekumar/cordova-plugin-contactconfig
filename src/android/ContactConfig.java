package com.contact.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import android.net.Uri;
import android.content.Intent;
import android.provider.ContactsContract;

public class ContactConfig extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("addContact")) {

            String name = data.getString(0);
            String message = "Successfully added "+name;

            callbackContext.success(message);
            return true;

        } else {
            
            return false;

        }
    }
}
