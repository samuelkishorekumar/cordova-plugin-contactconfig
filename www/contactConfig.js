/*global cordova, module yes man done man*/

module.exports = {
     addContact: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "ContactConfig", "addContact", [name]);
    }
};
